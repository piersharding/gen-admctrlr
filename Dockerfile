# Build the manager binary
FROM golang:1.20 as builder
ARG TARGETOS
ARG TARGETARCH

# Disable prompts from apt.
ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -yq --no-install-recommends python3-dev \
    && \
    ln -s /usr/lib/x86_64-linux-gnu/libpython3.11.so /usr/lib/x86_64-linux-gnu/libpython3.so && \
    /usr/sbin/ldconfig && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY cmd/main.go cmd/main.go
COPY api/ api/
COPY types/ types/

# Build
# the GOARCH has not a default value to allow the binary be built according to the host where the command
# was called. For example, if we call make docker-build in a local env which has the Apple Silicon M1 SO
# the docker BUILDPLATFORM arg will be linux/arm64 when for Apple x86 it will be linux/amd64. Therefore,
# by leaving it empty we can ensure that the container and binary shipped on it will have the same platform.
# ENV PKG_CONFIG_PATH /usr/lib/x86_64-linux-gnu/pkgconfig
RUN CGO_ENABLED=1 GO111MODULE=on GOOS=${TARGETOS:-linux} GOARCH=${TARGETARCH} \
     go build -a -o manager cmd/main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
# FROM gcr.io/distroless/static:nonroot
FROM golang:1.20
WORKDIR /

# Disable prompts from apt.
ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -yq --no-install-recommends python3-dev python3-pip python3-yaml \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

COPY --from=builder /workspace/manager .
USER 65532:65532

ENTRYPOINT ["/manager"]
