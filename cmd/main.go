/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

// // #cgo pkg-config: python3
// // #include <Python.h>
// // #cgo LDFLAGS: -lpython3.11
// import "C"

import (
	"flag"
	"fmt"
	"os"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.

	"k8s.io/apimachinery/pkg/runtime"
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	utilruntime "k8s.io/apimachinery/pkg/util/runtime"

	clientgoscheme "k8s.io/client-go/kubernetes/scheme"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	//+kubebuilder:scaffold:imports
	lbv1 "gitlab.com/piersharding/gen-admctrlr/api/v1"
	"gitlab.com/piersharding/gen-admctrlr/types"

	python "github.com/tliron/py4go"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("Setup/Initialisation")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	//+kubebuilder:scaffold:scheme
}

// etract the Python version
func pythonVersion() string {
	return fmt.Sprintf("Go >> Python version: %s", python.Version())
}

func main() {

	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "deadbeaf.skao.int",
		// LeaderElectionReleaseOnCancel defines if the leader should step down voluntarily
		// when the Manager ends. This requires the binary to immediately end when the
		// Manager is stopped, otherwise, this setting is unsafe. Setting this significantly
		// speeds up voluntary leader transitions as the new leader don't have to wait
		// LeaseDuration time first.
		//
		// In the default scaffold provided, the program ends immediately after
		// the manager stops, so would be fine to enable this option. However,
		// if you are doing or is intended to do any operation such as perform cleanups
		// after the manager stops then its usage might be unsafe.
		// LeaderElectionReleaseOnCancel: true,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	// Python integration -> https://github.com/tliron/py4go/tree/main
	// Set path to where /app.py is
	python.PrependPythonPath("/")

	// Initialize Python
	python.Initialize()
	defer python.Finalize()

	// What Python are we running
	setupLog.Info(pythonVersion())

	app, err := python.Import("app")
	defer app.Release()
	if err != nil {
		setupLog.Error(err, "Python Import failed!")
	} else {
		setupLog.Info("Python Import of app.py complete")
	}

	// Get access to AdmissionController module
	adm, err := app.GetAttr("AdmissionController")
	defer adm.Release()
	if err != nil {
		setupLog.Error(err, "Python failed to find app.AdmissionController!")
	} else {
		setupLog.Info("Python load of app.AdmissionController complete")
	}

	// Get access to a Python function
	pyinit, _ := adm.GetAttr("init")
	defer pyinit.Release()

	// Call the function with arguments
	cpyinit, err := pyinit.Call()
	defer cpyinit.Release()
	if err != nil {
		setupLog.Error(err, "Python init failed!")
	} else {
		setupLog.Info("Python init of app.py complete")
	}
	setupLog.Info(fmt.Sprintf("Returned: %s", cpyinit.String()))

	// Get access to a Python function
	types.PyCheck, err = adm.GetAttr("CheckOperaton")
	defer types.PyCheck.Release()
	if err != nil {
		setupLog.Error(err, "Python CheckOperaton not found!")
	} else {
		setupLog.Info("Python CheckOperaton in app.py")
	}
	types.PyRequest, err = adm.GetAttr("ProcessRequest")
	defer types.PyRequest.Release()
	if err != nil {
		setupLog.Error(err, "Python ProcessRequest not found!")
	} else {
		setupLog.Info("Python ProcessRequest in app.py")
	}

	mgr.GetWebhookServer().Register("/mutate-v1-all", &webhook.Admission{Handler: &lbv1.GenAnnotator{Client: mgr.GetClient()}})

	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
