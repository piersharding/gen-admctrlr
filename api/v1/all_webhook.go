/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License

// Go imports
package v1

import (
	"context"
	"fmt"

	"gitlab.com/piersharding/gen-admctrlr/types"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.

	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"sigs.k8s.io/controller-runtime/pkg/client"

	//+kubebuilder:scaffold:imports

	"gopkg.in/yaml.v3"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	logf "sigs.k8s.io/controller-runtime/pkg/log"

	"sync"
)

var (
	codecs       = serializer.NewCodecFactory(runtime.NewScheme())
	deserializer = codecs.UniversalDeserializer()
)

type GenAnnotator struct {
	Client  client.Client
	decoder *admission.Decoder
}

// +kubebuilder:webhook:path=/mutate-v1-all,mutating=true,failurePolicy=ignore,groups=apps;networking.k8s.io;batch,resources="*",verbs=create;update;delete,versions=v1,admissionReviewVersions=v1,sideEffects=None,name=mall.skao.int
// +kubebuilder:webhook:path=/mutate-v1-all,mutating=true,failurePolicy=ignore,groups="",resources=pods;services,verbs=create;update;delete,versions=v1,admissionReviewVersions=v1,sideEffects=None,name=msvc.skao.int

var lock sync.Mutex

// https://github.com/trstringer/kubernetes-mutating-webhook/blob/main/cmd/root.go
func (a *GenAnnotator) Handle(ctx context.Context, req admission.Request) admission.Response {
	log := logf.FromContext(ctx)

	body := fmt.Sprintf("%s", req.Object.Raw)
	log.Info("Request details: " + fmt.Sprintf("operation: %s(%s/%s/%s) %s/%s", req.Operation, req.Kind.Group, req.Kind.Version, req.Kind.Kind, req.Namespace, req.Name))
	log.Info("Raw request: ##" + body + "##")

	// Call the function with arguments
	op := fmt.Sprintf("%s", req.Operation)
	namespace := req.Namespace
	gv := fmt.Sprintf("%s/%s", req.RequestResource.Group, req.RequestResource.Version)
	kind := req.RequestResource.Resource
	name := req.Name
	gname := ""
	resource, err := YamlToMap(req.Object.Raw)
	if err != nil {
		log.Error(err, "YamlToMap failed!")
	}
	if kind == "pods" && name == "" {
		md := (resource.(map[string]interface{}))["metadata"]
		// iter := reflect.ValueOf(md).MapRange()
		// for iter.Next() {
		// 	k := iter.Key().Interface()
		// 	v := iter.Value().Interface()
		// 	log.Info(fmt.Sprintf("metadata [k/v]: %s => %s", k, v))
		// }
		gname = ((md.(map[string]interface{}))["generateName"]).(string)
		// name = fmt.Sprintf("%s(ReplicaSet -> generateName)", gname)
		// name = ((md.(map[string]interface{}))["name"]).(string)
		// log.Info(fmt.Sprintf("metadata [object]: %v", md))
	}

	// Note: if Kind is Pod and name is empty, it is probably part of a ReplicaSet
	log.Info(fmt.Sprintf("Request details: %s > %s -> %s %s/%s (gnerateName: %s)", op, gv, kind, namespace, name, gname))
	// var resource interface{}
	// err := yaml.Unmarshal(req.Object.Raw, &resource)
	// log.Info(fmt.Sprintf("resource [string]: %s", resource))
	// log.Info(fmt.Sprintf("resource [object]: %v", resource))

	// myMap := resource.(map[string]interface{})

	// result, err := json.Marshal(resource)
	// if err != nil {
	// 	log.Error(err, "json.Marshal failed!")
	// }
	// md := &map[string]interface{}{}
	// err = json.Unmarshal([]byte(result), md)
	// if err != nil {
	// 	log.Error(err, "json.Unmarshal failed!")
	// }
	// log.Info(fmt.Sprintf("md [string]: %s", *md))
	// log.Info(fmt.Sprintf("md [object]: %v", *md))
	// log.Info(fmt.Sprintf("metadata [object]: %v", (*md)["metadata"]))

	// if op != "DELETE" {
	// 	mdata := md["metadata"]
	// }

	// LOCK EVERYTHING!!!!
	lock.Lock()
	defer lock.Unlock()

	cpycheck, err := types.PyCheck.Call(op, gv, kind, namespace, name, gname)
	defer cpycheck.Release()
	if err != nil {
		log.Error(err, "Python PyCheck failed!")
		return admission.Allowed("Pod allowed")
	}
	if !cpycheck.ToBool() {
		log.Info(fmt.Sprintf("Python said decline processing: %s", cpycheck.String()))
		return admission.Allowed("Pod allowed")
	}
	log.Info(fmt.Sprintf("Request moving to ProcessRequest: %s > %s -> %s %s/%s (gnerateName: %s)", op, gv, kind, namespace, name, gname))

	// cpyproc, err := types.PyRequest.Call(op, gv, kind, namespace, name, string(req.Object.Raw))
	cpyproc, err := types.PyRequest.Call(op, gv, kind, namespace, name, body)
	defer cpyproc.Release()
	if err != nil {
		log.Info(fmt.Sprintf("Python PyRequest failed! %s", err))
		return admission.Allowed("Pod allowed")
	}
	sraw := cpyproc.String()
	log.Info(fmt.Sprintf("After to ProcessRequest: %s > %s -> %s %s/%s => %s", op, gv, kind, namespace, name, sraw))

	// 	log.Info("Python PyCheck call of controller complete")
	// 	// if cpycheck.IsList() {

	// 	// We create an empty array
	// 	// mylist := []string{}

	// 	// Unmarshal the json into it. this will use the struct tag
	// 	ret := cpycheck.String()
	// 	log.Info(fmt.Sprintf("ret val was: %s", ret))

	// 	var body interface{}
	// 	err := yaml.Unmarshal([]byte(ret), &body)
	// 	// err := json.Unmarshal([]byte(ret), &mylist)
	// 	if err != nil {
	// 		log.Info(fmt.Sprintf("Failed to get list"))
	// 	} else {
	// 		log.Info(fmt.Sprintf("List: %v", body))
	// 	}

	// 	// pop, _ := cpycheck.GetAttr("pop")
	// 	// defer pop.Release()
	// 	// for {
	// 	// 	itm, err := pop.Call(0)
	// 	// 	defer itm.Release()
	// 	// 	if err != nil {
	// 	// 		// log.Error(err, "No item!")
	// 	// 		break
	// 	// 		// } else {
	// 	// 		// 	log.Info("got an item")
	// 	// 	}
	// 	// 	log.Info(fmt.Sprintf("Returned itm: %s", itm.String()))
	// 	// }
	// 	// log.Info(fmt.Sprintf("Returned: %s", cpycheck.String()))
	// 	// } else {
	// 	// 	log.Info(fmt.Sprintf("Is not a List"))
	// 	// }
	// }
	// log.Info(fmt.Sprintf("Returned: %s", cpycheck.String()))

	// For converting from a string to a byte slice, string -> []byte:
	// []byte(str)

	// Decode the Pod from the AdmissionReview.
	// pod := corev1.Pod{}
	// if _, _, err := deserializer.Decode(req.Object.Raw, nil, &pod); err != nil {
	// 	log.Info("SOME OTHER OBJECT")
	// 	return admission.Errored(http.StatusBadRequest, err)
	// }

	// // mutate the fields in Pod
	// if pod.Annotations == nil {
	// 	pod.Annotations = map[string]string{}
	// }
	// log.Info("Annotated Pod: " + fmt.Sprintf("operation: %s %s/%s", req.Operation, pod.Namespace, pod.Name))
	// if req.Operation == "DELETE" {
	// 	log.Info("Pod: (RETURNING) " + fmt.Sprintf("operation: %s %s/%s", req.Operation, pod.Namespace, pod.Name))
	// 	return admission.Allowed("Delete Pod allowed")
	// }

	// if _, ok := pod.Annotations["skip-gen-admctrlr"]; ok {
	// 	log.Info("Pod: (RETURNING) " + fmt.Sprintf("annotations: %v %s/%s", pod.Annotations, pod.Namespace, pod.Name))
	// 	return admission.Allowed("Skip Pod fix")
	// }

	// var hasClaim bool = false
	// for _, v := range pod.Spec.Volumes {
	// 	if v.PersistentVolumeClaim != nil {
	// 		if v.PersistentVolumeClaim.ClaimName != "" {
	// 			hasClaim = true
	// 		}
	// 	}
	// }
	// if !hasClaim {
	// 	log.Info("Pod: (RETURNING NO CLAIM) " + fmt.Sprintf("annotations: %v %s/%s", pod.Annotations, pod.Namespace, pod.Name))
	// return admission.Allowed("Pod allowed")
	return admission.PatchResponseFromRaw(req.Object.Raw, []byte(sraw))
	// }

	// log.Info("Incoming Pod: volumes are - " + fmt.Sprintf("%v", pod.Spec.Volumes))
	// pod.Annotations["gen-admctrlr-mutating-admission-webhook"] = "set securityContext to root"
	// var nroot int64 = 0
	// pod.Spec.SecurityContext.RunAsUser = &nroot
	// pod.Spec.SecurityContext.RunAsGroup = &nroot
	// for i := range pod.Spec.Containers {
	// 	if pod.Spec.Containers[i].Lifecycle == nil {
	// 		pod.Spec.Containers[i].Lifecycle = &corev1.Lifecycle{PostStart: &corev1.LifecycleHandler{Exec: &corev1.ExecAction{}}}
	// 	}
	// 	if pod.Spec.Containers[i].Lifecycle.PostStart == nil {
	// 		pod.Spec.Containers[i].Lifecycle.PostStart = &corev1.LifecycleHandler{}
	// 	}
	// 	if pod.Spec.Containers[i].Lifecycle.PostStart.Exec == nil {
	// 		poutgoing Pod: spec: " + fmt.Sprintf("%v", pod.Spec))

	// log.Info("Pod: (PATCHED: set securityContext) " + fmt.Sprintf("operation: %s %s/%s", req.Operation, pod.Namespace, pod.Name))

	// marshaledPod, err := json.Marshal(pod)
	// if err != nil {
	// 	return admission.Errored(http.StatusInternalServerError, err)
	// }d.Spec.Containers[i].Lifecycle.PostStart.Exec = &corev1.ExecAction{}
	// 	}
	// 	pod.Spec.Containers[i].Lifecycle.PostStart.Exec.Command = append(pod.Spec.Containers[i].Lifecycle.PostStart.Exec.Command, "/bin/sh", "-c", "cp -f /usr/bin/true /usr/bin/chown; exit 0")
	// }
	// log.Info("O

	// // admission.Response
	// return admission.PatchResponseFromRaw(req.Object.Raw, marshaledPod)
}

func (a *GenAnnotator) InjectDecoder(d *admission.Decoder) error {
	a.decoder = d
	return nil
}

// YamlToMap converts a YAML string to a JSON string
// The input string is a byte array, and output is a generic interface{} struct
func YamlToMap(b []byte) (interface{}, error) {
	var body interface{}
	if err := yaml.Unmarshal(b, &body); err != nil {
		return nil, err
	}

	body = ConvertKeysToString(body)
	return body, nil
}

// recursive call to convert map keys
func ConvertKeysToString(i interface{}) interface{} {
	switch x := i.(type) {
	case map[interface{}]interface{}:
		m2 := map[string]interface{}{}
		for k, v := range x {
			m2[k.(string)] = ConvertKeysToString(v)
		}
		return m2
	case []interface{}:
		for i, v := range x {
			x[i] = ConvertKeysToString(v)
		}
	}
	return i
}
